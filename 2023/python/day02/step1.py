def lines_of_file(file_path: str) -> list:
    try:
        with open(file_path, 'r') as file:
            return file.read().splitlines()
    except FileNotFoundError:
        return list()


def get_game_number(line: str) -> int:
    return int(line.split('Game')[1].split(':')[0].strip())


def parse_line(line: str) -> list:
    clean_line = line.split(':')[1].replace(';', ',')
    return [term.strip() for term in clean_line.split(',')]


def is_game_possible(steps: list, available_cubes: dict) -> bool:
    for step in steps:
        number, color = step.split()
        if int(number) > available_cubes[color]:
            return False
    return True


FILE_PATH = 'file.txt'
lines = lines_of_file(FILE_PATH)


available_cubes = {
    'red': 12,
    'green': 13,
    'blue': 14
}


possible_games_ids = 0


for line in lines:
    game_number = get_game_number(line)
    steps = parse_line(line)
    if is_game_possible(steps, available_cubes):
        possible_games_ids += game_number


print(f'The sum of the IDs of the possible games is {possible_games_ids}')
