def lines_of_file(file_path: str) -> list:
    try:
        with open(file_path, 'r') as file:
            return file.read().splitlines()
    except FileNotFoundError:
        return list()


def parse_line(line: str) -> list:
    clean_line = line.split(':')[1].replace(';', ',')
    return [term.strip() for term in clean_line.split(',')]


def cubes_needed(steps: list) -> dict:
    cubes = {
        'red': 0,
        'green': 0,
        'blue': 0
    }
    for step in steps:
        number, color = step.split()
        if int(number) > cubes[color]:
            cubes[color] = int(number)
    return cubes


def power_of_cubes(cubes: dict) -> int:
    power = 1
    for number in cubes.values():
        power = power * number
    return power


FILE_PATH = 'file.txt'
lines = lines_of_file(FILE_PATH)


sum_of_powers = 0


for line in lines:
    steps = parse_line(line)
    cubes = cubes_needed(steps)
    power = power_of_cubes(cubes)
    sum_of_powers += power


print(f'The sum of the power of the mininum cubes needed for all games is {sum_of_powers}')
