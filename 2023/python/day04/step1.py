class Card:
    def __init__(self, line: str) -> None:
        self.line = line
        self.number = self.get_card_number()
        self.winning_numbers = self.get_winning_numbers()
        self.numbers_i_have = self.get_numbers_i_have()
        self.matching_numbers = self.get_matching_numbers()

    def get_card_number(self) -> int:
        try:
            return int(self.line.split('Card')[1].split(':')[0])
        except (IndexError, ValueError):
            return 0

    def get_winning_numbers(self) -> list:
        try:
            return self.line.split(':')[1].split('|')[0].split()
        except IndexError:
            return list()

    def get_numbers_i_have(self) -> list:
        try:
            return self.line.split(':')[1].split('|')[1].split()
        except IndexError:
            return list()

    def get_matching_numbers(self) -> list:
        matching_numbers = list()
        for number in self.winning_numbers:
            if number in self.numbers_i_have:
                matching_numbers.append(number)
        return matching_numbers


def lines_of_file(file_path: str) -> list:
    try:
        with open(file_path, 'r') as file:
            return file.read().splitlines()
    except FileNotFoundError:
        return list()


def calculate_points(matching_numbers: int) -> int:
    return 0 if matching_numbers == 0 else pow(2, matching_numbers - 1) 


FILE_PATH = 'file.txt'
lines = lines_of_file(FILE_PATH)
cards = [Card(line) for line in lines]


total_points = 0
for card in cards:
    total_points += calculate_points(len(card.matching_numbers))


print(f'The total number of points is {total_points}')
