def lines_of_file(file_path: str) -> list:
    try:
        with open(file_path, 'r') as file:
            lines = file.read().splitlines()
            matrix = list()
            for line in lines:
                matrix.append(list(line))
            return matrix
    except FileNotFoundError:
        return list()


def asterisk_coordinates(matrix: list) -> list:
    coordinates = list()
    for row in range(len(matrix)):
        for col in range(len(matrix[row])):
            if matrix[row][col] == '*':
                coordinates.append([row, col])
    return coordinates


def get_horizontal_part(line: list, col: int, direction: str) -> str:
    part = str()
    has_part_ended = False
    if direction == 'left':
        col -= 1
    while not has_part_ended:
        if col < 0:
            break
        try:
            char = line[col]
            if not char.isdigit():
                has_part_ended = True
            else:
                if direction == 'right':
                    part = part + char
                    col += 1
                elif direction == 'left':
                    part = char + part
                    col -= 1
                else:
                    has_part_ended = True
        except IndexError:
            has_part_ended = True
    return part


def complete_full_number(line: list, number_col: int) -> int:
    left_part = get_horizontal_part(line, number_col, 'left')
    right_part = get_horizontal_part(line, number_col, 'right')
    return int(left_part + right_part)


def get_adjacent_numbers(matrix: list, coordinates: list) -> list:
    row, col = coordinates
    modifyers = [
        [-1, -1],   [-1, +0],   [-1, +1],
        [+0, -1],               [+0, +1],
        [+1, -1],   [+1, +0],   [+1, +1] 
    ]
    numbers_rows = dict()
    for mod_row, mod_col in modifyers:
        calc_row, calc_col = row + mod_row, col + mod_col
        if calc_col < 0 or calc_row < 0:
            continue
        char = matrix[calc_row][calc_col]
        if char.isdigit():
            if mod_row in numbers_rows.values() and matrix[row + mod_row][col] not in ['.', '*']:
                continue
            full_number = complete_full_number(matrix[calc_row], calc_col)
            numbers_rows[full_number] = mod_row
    numbers = list(numbers_rows)
    return numbers if len(numbers) == 2 else list()


def calculate_gear_number(matrix: list, asterisk_coordinates: list) -> int:
    adjacent_numbers = get_adjacent_numbers(matrix, asterisk_coordinates)
    if not adjacent_numbers:
        return 0
    return adjacent_numbers[0] * adjacent_numbers[1]


FILE_PATH = 'file.txt'
matrix = lines_of_file(FILE_PATH)
coordinates = asterisk_coordinates(matrix)


gear_number_sum = 0
for coordinate in coordinates:
    gear_number = calculate_gear_number(matrix, coordinate)
    gear_number_sum += gear_number


print(f'The sum of all gear numbers is {gear_number_sum}')
