def lines_of_file(file_path: str) -> list:
    try:
        with open(file_path, 'r') as file:
            return file.read().splitlines()
    except FileNotFoundError:
        return list()


def three_lines(lines: list, i: int) -> tuple:
    previous_line = lines[i - 1] if i > 0 else str()
    actual_line = lines[i] if 0 <= i < len(lines) else str()
    next_line = lines[i + 1] if i < len(lines) - 1 else str()
    return previous_line, actual_line, next_line


def is_symbol(char):
    return char.isascii() and not char.isalnum() and char != '.'


def is_part_number(previous_line: str, actual_line: str, next_line: str, index_start: int, index_end: int) -> bool:
    lines = [previous_line, actual_line, next_line]
    for i in range(index_start - 1, index_end + 2):
        for line in lines:
            if 0 <= i < len(line) and is_symbol(line[i]):
                return True
    return False


def sum_line_part_numbers(previous_line: str, actual_line: str, next_line: str) -> int:
    part_number_sum = 0
    number = ''
    i = 0
    has_line_ended = False
    while not has_line_ended:
        if actual_line[i].isdigit():
            number = ''
            j = i
            index_start = j
            while j < len(actual_line) and actual_line[j].isdigit():
                number += actual_line[j]
                j += 1
            index_end = j - 1
            if is_part_number(previous_line, actual_line, next_line, index_start, index_end):
                part_number_sum += int(number)
            i = j
        else:
            i += 1
        has_line_ended = not (i < len(actual_line))
    return part_number_sum


FILE_PATH = 'file.txt'
lines = lines_of_file(FILE_PATH)


part_number_sum = 0
for i in range(0, len(lines)):
    previous_line, actual_line, next_line = three_lines(lines, i)
    part_number_sum += sum_line_part_numbers(previous_line, actual_line, next_line)


print(f'The sum of all part numbers is {part_number_sum}')
