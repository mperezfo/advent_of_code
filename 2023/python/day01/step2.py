DIGITS = {
    "one":      1,
    "two":      2,
    "three":    3,
    "four":     4,
    "five":     5,
    "six":      6,
    "seven":    7,
    "eight":    8,
    "nine":     9,
    "zero":     0
}


def lines_of_file(file_path: str) -> list:
    try:
        with open(file_path, 'r') as file:
            return file.read().split()
    except FileNotFoundError:
        return list()


def first_line_number(line: str, is_inverted: bool=False) -> int:
    checked_characters = str()
    for character in line:
        for key, value in DIGITS.items():
            if (not is_inverted and key in checked_characters) or (is_inverted and key in checked_characters[::-1]):
                return value
        try:
            return int(character)
        except ValueError:
            checked_characters += character
    return -1


def calibration_number(line: str) -> int:
    final_number = str()
    first_number = first_line_number(line)
    last_number = first_line_number(line[::-1], is_inverted=True)
    for number in [first_number, last_number]:
        if number != -1:
            final_number += str(number)
    return int(final_number) if final_number != str() else 0


def sum_calibration_numbers(lines: list) -> int:
    final_sum = 0
    for line in lines:
        final_sum += calibration_number(line)
    return final_sum


FILE_PATH = 'file.txt'
lines = lines_of_file(FILE_PATH)


if not lines:
    print(f'No lines found in file {FILE_PATH}')
else:
    final_sum = sum_calibration_numbers(lines)
    print(f'The sum of all calibration number is {final_sum}')
