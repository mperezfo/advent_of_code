def lines_of_file(file_path: str) -> list:
    try:
        with open(file_path, 'r') as file:
            return file.read().split()
    except FileNotFoundError:
        return list()


def first_line_number(line: str) -> int:
    for character in line:
        try:
            return int(character)
        except ValueError:
            continue
    return -1


def calibration_number(line: str) -> int:
    final_number = str()
    first_number = first_line_number(line)
    last_number = first_line_number(line[::-1])
    for number in [first_number, last_number]:
        if number != -1:
            final_number += str(number)
    return int(final_number) if final_number != str() else 0


def sum_calibration_numbers(lines: list) -> int:
    final_sum = 0
    for line in lines:
        final_sum += calibration_number(line)
    return final_sum


FILE_PATH = 'file.txt'
lines = lines_of_file(FILE_PATH)


if not lines:
    print(f'No lines found in file {FILE_PATH}')
else:
    final_sum = sum_calibration_numbers(lines)
    print(f'The sum of all calibration number is {final_sum}')
