package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

func pop(list []int, i int) []int {
	list[i] = list[len(list)-1]
	return list[:len(list)-1]
}

func getLists(fileName string) ([]int, []int, error) {
	data, err := os.ReadFile(fileName)
	if err != nil {
		return nil, nil, err
	}
	body := string(data)
	numberOfLines := strings.Count(body, "\n")
	listOne := make([]int, numberOfLines+1)
	listTwo := make([]int, numberOfLines+1)
	for i, line := range strings.Split(strings.TrimSuffix(body, "\n"), "\n") {
		values := strings.Fields(line)
		valueOne, err := strconv.Atoi(values[0])
		if err != nil {
			return nil, nil, err
		}
		valueTwo, err := strconv.Atoi(values[1])
		if err != nil {
			return nil, nil, err
		}
		listOne[i] = valueOne
		listTwo[i] = valueTwo
	}
	return listOne, listTwo, nil
}

func diff(a, b int) int {
	if a < b {
		return b - a
	}
	return a - b
}

func minAndIdex(list []int) (int, int) {
	if len(list) == 0 {
		panic("slice is empty")
	}
	min := list[0]
	minIndex := 0
	for i, number := range list {
		if number < min {
			min = number
			minIndex = i
		}
	}
	return min, minIndex
}

func minIntDiff(listOne, listTwo *[]int) int {
	minOne, indexOne := minAndIdex(*listOne)
	minTwo, indexTwo := minAndIdex(*listTwo)
	*listOne = pop(*listOne, indexOne)
	*listTwo = pop(*listTwo, indexTwo)
	fmt.Printf("minOne: %v, minTwo: %v, diff: %v\n", minOne, minTwo, diff(minOne, minTwo))
	return diff(minOne, minTwo)
}

func main() {
	const FILE_NAME = "file.txt"
	listOne, listTwo, err := getLists(FILE_NAME)
	if err != nil {
		panic(err)
	}
	var listDiff int
	for len(listOne) > 0 {
		listDiff += minIntDiff(&listOne, &listTwo)
	}
	fmt.Printf("listDiff: %v", listDiff)
}
